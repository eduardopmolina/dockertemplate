# DockerTemplate

Here is a .gitlab-ci.yml file that you can drop in directly without any modification in a project with a working Dockerfile.

.gitlab-common.yml file is usd to store required or optional variables.

It will:

lint your image if requested with hadolint, however you can use your own.
build a docker image for each git commit, tagging the docker image
tag the docker image “latest” for the “master” branch
keep in sync git tags with docker tags

Note: All docker images may be pushed to the GitLab registry containing the project

Best practices
Do not use “latest” nor “stable” images when using a CI. Why? Because you want reproducibility. You want your pipeline to work in 6 month. Or 6 years. Latest images will break things.
Always target a version. Hence image: docker:18 here.

To speed-up your docker builds, pull the “latest” image ($CI_REGISTRY_IMAGE:latest) before building , and then build with --cache-from $CI_REGISTRY_IMAGE:latest. 
This will make sure the GitLab runner has the latest image and can leverage layer caching. Chances are you did not change all layers, so the build process will be very fast.

In the push jobs, tell GitLab not to clone the source code with GIT_STRATEGY: none. Since we are just playing with docker pull/push, we do not need the source code. 
This will speed things up as well.

Finally, keep your Git tags in sync with your Docker tags. If you have not automated this, you have probably found yourself in the situation of wondering “which git tag is 
this image again?”. No more. Use GitLab “tags” pipelines.


